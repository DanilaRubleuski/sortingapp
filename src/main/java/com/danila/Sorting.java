package com.danila;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Main Sorting class.
 */
public class Sorting
{
    /**
     * Array printing method.
     * @param array Given array to print
     */
    public void printArray(int[] array){
        System.out.println(Arrays.toString(array));
    }

    /**
     * Method to check if the given array is <b>already</b> sorted.
     * @param array Given array
     * @param length Array length
     * @return True if the array is sorted, False if not
     */
    boolean isSorted(int[] array, int length) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1])
                return false;
        }
        return true;
    }

    /**
     * Sorting method.
     * @param array Given array to sort
     */
    public void sort(int[] array){
        if(array ==null || array.length == 0)
            throw new IllegalArgumentException("The given array is null or has no elements.");
        else if(array.length>10)
            throw new IllegalArgumentException("Size of array is greater than 10");

        if(array.length==1 || isSorted(array,array.length))
                return;
        else{
            for (int i = 0; i < array.length - 1; i++) {
                for (int j = 0; j < array.length - i - 1; j++) {
                    if (array[j + 1] < array[j]) {

                        int swap = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = swap;

                    }
                }
            }
        }
        printArray(array);
    }

    /**
     * Main method.
     * @param args
     */
    public static void main( String[] args )
    {
        Scanner sc=new Scanner(System.in);

        while(true){
            System.out.println("Enter the array length:");
            int length = sc.nextInt();
            if(length < 1){
                System.out.println("Array could not have 0 or less elements!");
                continue;
            }else if(length>10){
                System.out.println("Array size should be less than 10!");
                continue;
            }

            int[] array = new int[length];
            System.out.println("Enter the array elements:");
            for (int i = 0; i<array.length; i++)
                array[i] = sc.nextInt();
            
            Sorting sorting = new Sorting();
            System.out.println("Sorted array:");
            sorting.sort(array);
            break;
        }
    }
}
