package com.danila;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

/**
 * Unit test for a one-element array.
 */
@RunWith(Parameterized.class)
public class ArrayOneElementCaseTest {
    Sorting sorting = new Sorting();
    private int[] array;
    private int[] expected;

    /**
     * <b>ArrayOneElementCase</b> Test Constructor method.
     * @param array Tested array
     * @param expected Expected array
     */
    public ArrayOneElementCaseTest(int[] array, int[] expected){
        this.array = array;
        this.expected = expected;
    }

    /**
     * Test checking if the one-element array will be the same after <b>sort()</b> method.
     */
    @Test
    public void testOneElementArrayCase(){
        sorting.sort(this.array);
        assertArrayEquals(expected,this.array);
    }
    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new int[][][] {
                {{1},{1}},
                {{-123},{-123}},
                {{30},{30}},
                {{-2147483646},{-2147483646}},
                {{0},{0}}
        });
    }
}
