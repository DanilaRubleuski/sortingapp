package com.danila;

import org.junit.Test;
/**
 * Unit test for a null array.
 */
public class ArrayNullCaseTest {
    Sorting sorting = new Sorting();

    /**
     * Test checking if the IllegalArgumentException() will be thrown for the <b>null</b> array.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testNullCase(){
        sorting.sort(null);
    }
}
