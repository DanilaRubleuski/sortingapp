package com.danila;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

/**
 * Unit test for a regular array, that meets the condition(not null, 1 to 10 elements, not pre-sorted).
 */
@RunWith(Parameterized.class)
public class ArrayRegularCaseTest {
    Sorting sorting = new Sorting();
    private int[] array;
    private int[] expected;

    /**
     * <b>ArrayRegularCase</b> Test Constructor method.
     * @param array Tested array
     * @param expected Expected array
     */
    public ArrayRegularCaseTest(int[] array, int[] expected){
        this.array = array;
        this.expected = expected;
    }

    /**
     * Test checking if the regular array will be properly sorted after <b>sort()</b> method.
     */
    @Test
    public void testRegularArrayCase(){
        sorting.sort(this.array);
        assertArrayEquals(expected,this.array);
    }
    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new int[][][] {
                {{5,4,3,2,1},{1,2,3,4,5}},
                {{56,342,12,3,0,87,4,964},{0,3,4,12,56,87,342,964}},
                {{1,2,1,1,1,1,1,1,1,1},{1,1,1,1,1,1,1,1,1,2}},
                {{67,12,19,11,1,82,100,99,36},{1,11,12,19,36,67,82,99,100}},
                {{2,3,1},{1,2,3}},
                {{1000000,0,3,2,1,99999},{0,1,2,3,99999,1000000}}
        });
    }
}
