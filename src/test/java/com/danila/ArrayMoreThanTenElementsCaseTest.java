package com.danila;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
/**
 * Unit test for an array with more than 10 elements.
 */
@RunWith(Parameterized.class)
public class ArrayMoreThanTenElementsCaseTest {

    Sorting sorting = new Sorting();
    private int[] array;

    /**
     * <b>ArrayMoreThanTenElementsCase</b> Test Constructor method.
     * @param array Tested array
     */
    public ArrayMoreThanTenElementsCaseTest(int[] array){
        this.array = array;
    }

    /**
     * Test checking if the IllegalArgumentException() will be thrown for the array with <b>more</b> than 10 elements.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenElementsArrayCase(){
        sorting.sort(this.array);
    }

    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new int[][][] {
                {{1,2,3,4,5,6,7,8,9,10,11}},
                {{88,55,44,33,22,11,77,88,99,100,111}},
                {{67,84,241,7563,3,8980,34,12,8,6,45}},
                {{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}},
                {{60,59,58,57,0,56,55,53,54,52,51,1,2,3,4,5,6,7,8,9,10}},
                {{0,0,0,0,0,0,0,0,0,0,0}}
        });
    }
}
