package com.danila;

import org.junit.Test;
/**
 * Unit test for an empty array.
 */
public class ArrayNoElementsCaseTest {
    Sorting sorting = new Sorting();

    /**
     * Test checking if the IllegalArgumentException() will be thrown for the array with <b>no elements</b>.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testNoElementsCase(){
        sorting.sort(new int[]{});
    }
}
