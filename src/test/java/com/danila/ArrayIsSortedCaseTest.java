package com.danila;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

/**
 * Unit test for a sorted array.
 */
@RunWith(Parameterized.class)
public class ArrayIsSortedCaseTest {
    Sorting sorting = new Sorting();
    private int[] array;
    private int[] expected;

    /**
     * <b>ArrayIsSortedCase</b> Test Constructor method.
     * @param array Tested array
     * @param expected Expected array
     */
    public ArrayIsSortedCaseTest(int[] array, int[] expected){
        this.array = array;
        this.expected = expected;
    }

    /**
     * Test checking if the sorted array will be the same after <b>sort()</b> method.
     */
    @Test
    public void testSortedArrayCase(){
        sorting.sort(this.array);
        assertArrayEquals(expected,this.array);
    }
    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new int[][][] {
                {{1,2,3,4,5},{1,2,3,4,5}},
                {{2,2,2,2,2,2,2,2,2,2},{2,2,2,2,2,2,2,2,2,2}},
                {{-2147483646,-1,0,5,96,128,204,377,395,412},{-2147483646,-1,0,5,96,128,204,377,395,412}},
                {{0,1,2147483646},{0,1,2147483646}},
                {{-100,-50,-10,-1,0,1,10,50,100},{-100,-50,-10,-1,0,1,10,50,100}}
        });
    }
}
